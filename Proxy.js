const http = require('http');
const net = require('net');
const dns = require('dns');
const {BindingAgent, createConnection} = require('./BindingAgent');

class Proxy {
    constructor(options) {
        this._localAddress = options.localAddress;
        this._agent = new BindingAgent({
            localAddress: options.localAddress,
        });
        this._server = http.createServer();
        this._server.on('request', this.onRequest.bind(this));
        this._server.on('connect', this.onConnect.bind(this));
        this._server.listen(options.port);
    }

    onRequest(client_req, client_res) {
        let [host, port] = client_req.headers.host.split(':');
        dns.lookup(host, {
            family: net.isIP(this._localAddress),
        }, (err, address) => {
            if(!address) {
                client_res.writeHead(502);
                client_res.end();
            }
            else {
                var options = {
                    method: client_req.method,
                    host: address,
                    port: port || 80,
                    path: client_req.url,
                    headers: client_req.headers,
                    agent: this._agent,
                };

                var proxy = http.request(options, (res) => {
                    res.pipe(client_res, {
                        end: true
                    });
                });

                client_req.pipe(proxy, {
                    end: true
                });
            }
        });
    }

    onConnect(req, socket, head) {
        let [host, port] = req.headers.host.split(':');

        dns.lookup(host, {
            family: net.isIP(this._localAddress),
        }, (err, address) => {
            if(!address) {
                socket.write(`HTTP/${req.httpVersion} 502 Host not found\r\n\r\n`, 'UTF-8', () => {
                    socket.end();
                });
            }
            else {
                const conn = createConnection({
                    host: address,
                    port: port || 443,
                    localAddress: this._localAddress,
                }, function() {
                    socket.write(`HTTP/${req.httpVersion} 200 OK\r\n\r\n`, 'UTF-8', () => {
                        conn.pipe(socket);
                        socket.pipe(conn);
                    });
                });

                conn.on('error', function(e) {
                    console.log("Server connection error: " + e);
                    socket.end();
                });
            }
        });
    }
};

module.exports = Proxy;
