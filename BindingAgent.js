const http = require('http');
const net = require('net');
const FFI = require('ffi');


var SOL_SOCKET = 1;
var AF_INET = 2;
var AF_INET6 = 10;
var SOCK_STREAM = 1;
const IPPROTO_IP = 0;
const SO_BINDTODEVICE = 25;
const IP_FREEBIND = 15;
var ref = require('ref');
const ffi = FFI.Library(null, {
    setsockopt: ['int', ['int', 'int', 'int', 'string', 'int']],
    socket: ['int', ['int', 'int', 'int']],
});
var intvlVal = ref.alloc('int', 1);
var intvlValLn = intvlVal.type.size;

function getIPDomain(ip) {
    const family = net.isIP(ip);
    if(family === 4)
        return AF_INET;
    if(family === 6)
        return AF_INET6;
    throw 'Invalid IP';
}

function createConnection(options, callback) {
    const fd = ffi.socket(getIPDomain(options.host), SOCK_STREAM, 0);
    ffi.setsockopt(fd, IPPROTO_IP, IP_FREEBIND, intvlVal, intvlValLn);

    return net.createConnection({
        fd: fd,
        host: options.host,
        port: options.port,
        localAddress: options.localAddress,
    }, callback);
}

class BindingAgent extends http.Agent {
    constructor(options) {
        super(options);
        this._localAddress = options.localAddress;
    }

    createConnection(options, callback) {
        return createConnection(options, callback);
    }
};
module.exports = {
    BindingAgent,
    createConnection
};
