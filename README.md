# iproxy


## Add an IP
If you can use multiple IPs on the same interface (for instance in the case of failovers), you can add this IP in the ```/etc/network/interfaces``` on debian :
```
auto eth0:0
iface eth0:0 inet static
address IP_FAILOVER1
netmask 255.255.255.255
post-up /sbin/ifconfig eth0:0 IP_FAILOVER1 netmask 255.255.255.255 broadcast IP_FAILOVER1
pre-down /sbin/ifconfig eth0:0 down
```
For other systems, look at https://docs.ovh.com/fr/dedicated/network-ipaliasing/  
You can also add an IPv6 https://docs.ovh.com/fr/dedicated/network-ipv6/

## Add an IPv6 block
IPv6 is a great way to get many IPs. If you got an IPv6 block you can easily create many IPs for the same host.  
And that's were bindproxy shines, IP_FREEBIND allows us to bind to any IP routed to a system, regardless of having a (virutal) interface for this IP or not. No need to add every IPv6 manually in the /etc/network/interfaces files !   
Just create /etc/dhcp/dhclient6.conf file :
```
interface "eth0" {
    send dhcp6.client-id 13:37:13:37:13:37:13:37:13:37;
}
```

create system.d service `/etc/systemd/system/dhclient.service` :
```
[Unit]
Description=dhclient for sending DUID IPv6
Wants=network.target
Before=network.target

[Service]
Type=forking
ExecStart=/usr/sbin/dhclient -cf /etc/dhcp/dhclient6.conf -6 -P -v eth0

[Install]
WantedBy=multi-user.target
```

and enable+start the service :
```
systemctl enable dhclient.service
systemctl start dhclient.service
```

## Install
Install almost like any nodejs app.
```
sudo apt-get install build-essential
npm install
```

## Install daemon
```
cp bindproxy.service /lib/systemd/system/
$EDITOR /lib/systemd/system/bindproxy.service  
systemctl enable bindproxy
systemctl start bindproxy
```
