const Proxy = require('./Proxy');
const fs = require('fs');
const config = JSON.parse(fs.readFileSync('./config.json'));

for(let [ip, port] of Object.entries(config.bindings)) {
    const proxy = new Proxy({
        localAddress: ip,
        port: port,
    });
    console.log(`Started proxy server at port ${port}, binding with ip address ${ip}`);
}
